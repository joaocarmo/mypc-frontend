import React, { useCallback, useEffect, useState } from 'react'
import Message from '../message'
import * as config from '../../config'

const Messages = ({ lastUpdate, setLastUpdate }) => {
  const [messages, setMessages] = useState([])
  const [error, setError] = useState(null)

  const getMessages = useCallback(async () => {
    try {
      const response = await fetch(config.API_MESSAGES``)

      if (response.ok) {
        const data = await response.json()

        setMessages(data.reverse())
      }
    } catch (err) {
      setError(err?.message)
    }
  }, [])

  useEffect(() => {
    getMessages()
  }, [lastUpdate])

  if (error) {
    return (
      <div className="alert alert-danger" role="alert">
        {error}
      </div>
    )
  }

  return (
    <ul className="list-group">
      {messages.map(({ _id, text }) => (
        <Message key={_id} id={_id} setLastUpdate={setLastUpdate}>
          {text}
        </Message>
      ))}
    </ul>
  )
}

export default Messages
