import React, { useCallback, useState } from 'react'
import * as config from '../../config'

const Input = ({ setLastUpdate }) => {
  const [message, setMessage] = useState('')
  const [error, setError] = useState(null)

  const submitMessage = useCallback(async (text) => {
    try {
      const options = {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ text }),
      }
      const response = await fetch(config.API_MESSAGES``, options)

      if (response.ok) {
        setMessage('')
        setLastUpdate(new Date())
      }
    } catch (err) {
      setError(err?.message)
    }
  }, [])

  const handleOnChange = useCallback((event) => {
    setMessage(event?.target?.value || '')
  }, [])

  const handleOnClick = useCallback(() => {
    submitMessage(message)
  }, [message, submitMessage])

  if (error) {
    return (
      <div className="alert alert-danger" role="alert">
        {error}
      </div>
    )
  }

  return (
    <div className="input-group mb-3">
      <div className="input-group-prepend">
        <span className="input-group-text" id="new-message">
          New message
        </span>
      </div>
      <input
        type="text"
        className="form-control"
        placeholder="What's on your mind?"
        aria-label="New message"
        aria-describedby="new-message"
        value={message}
        onChange={handleOnChange}
      />
      <div className="input-group-append">
        <button
          className="btn btn-outline-secondary"
          type="button"
          id="new-message-send"
          onClick={handleOnClick}
        >
          Send
        </button>
      </div>
    </div>
  )
}

export default Input
