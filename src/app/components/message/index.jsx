import React, { useCallback, useState } from 'react'
import * as config from '../../config'

const Message = ({ id, children, setLastUpdate }) => {
  const [error, setError] = useState(null)

  const deleteMessage = useCallback(async (id) => {
    try {
      const options = {
        method: 'DELETE',
      }
      const response = await fetch(config.API_MESSAGES`${id}`, options)

      if (response.ok) {
        setLastUpdate(new Date())
      }
    } catch (err) {
      setError(err?.message)
    }
  }, [])

  const handleOnClick = useCallback(() => {
    deleteMessage(id)
  }, [id, deleteMessage])

  if (error) {
    return (
      <li className="list-group-item">
        <div className="alert alert-danger" role="alert">
          {error}
        </div>
      </li>
    )
  }

  return (
    <li
      className="list-group-item d-flex justify-content-between align-items-center"
    >
      {children}
      <button
        type="button"
        className="close"
        title="Delete"
        aria-label="Delete"
        onClick={handleOnClick}
      >
        <span aria-hidden="true">&times;</span>
      </button>
    </li>
  )
}

export default Message
