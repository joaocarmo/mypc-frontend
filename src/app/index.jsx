import React, { useState } from 'react'
import Messages from './components/messages'
import InputMessage from './components/input-message'

const App = () => {
  const [lastUpdate, setLastUpdate] = useState(new Date())

  return (
    <div className="container mt-3">
      <div className="row m-3">
        <div className="col-sm">
          <div className="alert alert-secondary" role="alert">
            <strong>Last update: </strong>
            <time>{lastUpdate.toLocaleString()}</time>
          </div>
        </div>
      </div>
      <div className="row m-3">
        <div className="col-sm">
          <InputMessage setLastUpdate={setLastUpdate} />
        </div>
      </div>
      <div className="row m-3">
        <div className="col-sm">
          <Messages lastUpdate={lastUpdate} setLastUpdate={setLastUpdate} />
        </div>
      </div>
    </div>
  )
}

export default App
