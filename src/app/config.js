export const API_PATH = `/api`

export const API_MESSAGES = (strings, id) =>
  id ? `${API_PATH}/messages/${id}` : `${API_PATH}/messages`
