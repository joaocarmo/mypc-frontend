# mypc-frontend

Frontend POC using [React](https://reactjs.org).

## Deployment

```sh
# Build docker image (tag: mypc-frontend)
docker build -t mypc-frontend .

# Start one instance of the docker image
docker run -p 3000:80 mypc-frontend
```

The app will be available at <http://localhost:3000> and is self-contained.
