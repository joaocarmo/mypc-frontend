FROM node:16-alpine AS builder

WORKDIR /opt/mypc

COPY package.json yarn.lock ./

RUN yarn --frozen-lockfile

# Use npm modules with binaries
ENV PATH /opt/mypc/node_modules/.bin:$PATH

COPY . .

FROM builder AS build

RUN yarn build

FROM nginx:1.19-alpine

COPY ./nginx.config /etc/nginx/conf.d/default.conf

COPY --from=build /opt/mypc/dist /usr/share/nginx/html
